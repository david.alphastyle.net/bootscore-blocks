<?php

/**
 * Hero Text Left Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */
// Create id attribute allowing for custom "anchor" value.
$id = 'hero-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'hero';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
$title = get_field('title') ?: 'Your title here...';
$description = get_field('description') ?: 'Description Here';
$url = get_field('url') ?: 'Landing page';
$image = get_field('image') ?: 'Nhập ảnh tại đây';
$button_title = get_field('button_title') ?: 'Nhập nhãn nút bấm';

 ?>





  <div id="<?php echo esc_attr($id); ?>" class="container col-xxl-8 px-4 py-5">
    <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
      <div class="col-10 col-sm-8 col-lg-6">
        <img src="<?php echo esc_url($image['url']); ?>" class="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="700" height="500" loading="lazy">
      </div>
      <div class="col-lg-6">
        <h1 class="display-5 fw-bold lh-1 mb-3"><?php echo $title; ?></h1>
        <p class="lead"><?php echo $description; ?></p>
        <div class="d-grid gap-2 d-md-flex justify-content-md-start">
        <a href="<?php echo $url; ?>" type="button" class="btn btn-primary btn-lg px-4 gap-3"><?php echo $button_title; ?></a>
        </div>
      </div>
    </div>
  </div>