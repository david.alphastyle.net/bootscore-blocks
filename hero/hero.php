<?php

/**
 * Hero Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */
// Create id attribute allowing for custom "anchor" value.
$id = 'hero-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'hero';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
$title = get_field('title') ?: 'Your title here...';
$description = get_field('description') ?: 'Description Here';
$url = get_field('url') ?: 'Landing page';
$image = get_field('image') ?: 'Nhập ảnh tại đây';

 ?>


<div id="<?php echo esc_attr($id); ?>" class="px-4 pt-5 my-5 text-center border-bottom">
    <h1 class="display-4 fw-bold"><?php echo $title; ?></h1>
    <div class="col-lg-6 mx-auto">
      <p class="lead mb-4"><?php echo $description; ?></p>
      <div class="d-grid gap-2 d-sm-flex justify-content-sm-center mb-5">
      <a href="<?php echo $url; ?>" type="button" class="btn btn-primary btn-lg px-4 gap-3">Primary button</a>
      </div>
    </div>
    <div class="overflow-hidden" style="max-height: 30vh;">
      <div class="container px-5">
        <img src="<?php echo esc_url($image['url']); ?>" class="img-fluid border rounded-3 shadow-lg mb-4" alt="Example image" width="700" height="500" loading="lazy">
      </div>
    </div>
  </div>