<?php

/**
 * Slider Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'slider-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'slider';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>
<div id="<?php echo esc_attr($id); ?>" class="carousel carousel-dark slide" data-bs-ride="carousel">
    <?php if( have_rows('slides') ): ?>
       

		<div class="carousel-inner">
        <?php $i = 0; ?>
			<?php while( have_rows('slides') ): the_row(); 
				$image = get_sub_field('image');
                $title = get_sub_field('title');
                $description = get_sub_field('description');
                $url = get_sub_field('url');
                $button_text = get_sub_field('button_text');

				?>
				<div class="carousel-item<?php $i++; if($i == 1) { echo ' active'; } ?>">
                <img class="bd-placeholder-img d-block img-fluid h-100" src="<?php echo esc_url($image['url']); ?>" alt="First slide" height="100%" width="100%">    
                <div class="container">
                <div class="carousel-caption text-start d-none d-md-block">
                <h5><?php echo $title ?></h5>
                <p><?php echo $description ?></p>
        <?php 
                    if ($url)  {?>
                    <a href="<?php echo $url?>" type="button" class="btn btn-primary"><?php echo $button_text?></a>
                    <?php } ?>
                    </div>

                    </div>

				</div>
			<?php endwhile; ?>
		</div>
        <button class="carousel-control-prev" type="button" data-bs-target="#<?php echo esc_attr($id); ?>" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#<?php echo esc_attr($id); ?>" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
              <!-- Indicators -->
              <div class="carousel-indicators">
<?php 
for($x=0; $x<$i; $x++){ ?>
<button type="button" data-bs-target="#<?php echo esc_attr($id); ?>" data-bs-slide-to="<?php echo $x; ?>" class="<?php if($x==0){ echo 'active'; } ?> " aria-label="Slide <?php echo $x; ?>"></button>    
<?php    }



?>

</div>
	<?php else: ?>
		<p>Please add some slides.</p>
	<?php endif; ?>


</div>