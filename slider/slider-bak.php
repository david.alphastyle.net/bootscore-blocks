<?php

/**
 * Slider Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'slider-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'slider';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <?php if( have_rows('slides') ): ?>
		<div class="slides">
			<?php while( have_rows('slides') ): the_row(); 
				$image = get_sub_field('image');
                $text = get_sub_field('text');
                $url = get_sub_field('url');
				?>
				<div class="px-4 py-5 my-5 text-center" style="background-image:url('<?php echo esc_url($image['url']); ?>');background-repeat:no-repeat;background-size:cover;height:700px">
                    <p class="display-3"><?php echo $text ?></p>
                   <?php 
                    if ($url)  {?>
                    <a href="<?php echo $url?>" type="button" class="btn btn-primary">Primary</a>
                    <?php } ?>
				</div>
			<?php endwhile; ?>
		</div>
	<?php else: ?>
		<p>Please add some slides.</p>
	<?php endif; ?>
</div>