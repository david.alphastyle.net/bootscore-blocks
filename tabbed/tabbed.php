<?php

/**
 * Tabbed Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */
// Create id attribute allowing for custom "anchor" value.
$id = 'tabbed-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'tabbed';
if( !empty($block['tabbed']) ) {
    $className .= ' ' . $block['tabbed'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}


 ?>

<?php 
									$rows = get_field('tab_test');
                                            $x = 0;
											
											if (have_rows('tab_test')) {

                                               echo '<div class="d-flex align-items-start custom-flex-tabbed">'; 
											   echo'<nav class="nav nav-pills mb-3 me-2 custom-flex-column" role="tablist" id="'.esc_attr($id).'" aria-orientation="vertical">';
											  
											  while(have_rows('tab_test')) { the_row();
											    
											   $tab_button = get_sub_field('tab_button');
											    $tab_id = get_sub_field('tab_id');
											  
											   
											    
                                                ?>
                                                <button id="<?php echo $tab_id ?>-tab" aria-controls="<?php echo $tab_button ?>" role="tab" data-bs-toggle="pill" data-bs-target="#<?php echo $tab_id ?>" type="button" role="tab" class="nav-link <?php $x++; if($x == 1) { echo 'active'; } ?>"> 
                                                 <?php
											    // output sub field content
											    echo''.$tab_button.'';
											    
											    echo'</button>';
											    
											  }
											  echo'</nav>';
											  // reset the rows of the repeater
											  reset_rows();
											  echo'<div class="tab-content w-80 m-auto" id="'.esc_attr($id).'Content">';
											  $i = 0;
											  // second loop of same rows
											  while(have_rows('tab_test')) { the_row();
											  
											  
											     $tab_button = get_sub_field('tab_button');
											    $tab_content = get_sub_field('tab_content');
											    $tab_id = get_sub_field('tab_id');
                                                
                                                ?>
                                                <div role="tabpanel" class="tab-pane fade <?php $i++; if($i == 1) { echo 'show active'; } ?>" id="<?php echo $tab_id?>">

                                                <?php echo $tab_content ?>
                                              </div>

                                              <?php      
											  }
											  echo'</div>';
											  echo'</div>';
											}						
									 ?>