# Bootscore Code Block

## Description
This plugins require ACF Pro Block to Run properly

## How to Use

### Step 1: Enqueue Scripts (Register Block)
```
// Register a slider block.
add_action('acf/init', 'my_register_blocks');
function my_register_blocks() {

    // check function exists.
    if( function_exists('acf_register_block_type') ) {

        // register a slider block.
        acf_register_block_type(array(
            'name'              => 'slider',
            'title'             => __('Slider'),
            'description'       => __('A custom slider block.'),
            'render_template'   => 'blocks/slider/slider.php',
			'category'          => 'formatting',
			'icon' 				=> 'images-alt2',
			'align'				=> 'full',
			//'enqueue_assets' 	=> function(){
				//wp_enqueue_style( 'slick', 'http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', array(), '1.8.1' );
				//wp_enqueue_style( 'slick-theme', 'http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css', array(), '1.8.1' );
				//wp_enqueue_script( 'slick', 'http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array('jquery'), '1.8.1', true );

				//wp_enqueue_style( 'block-slider', get_stylesheet_directory_uri(). '/blocks/slider/slider.css', array(), '1.0.0' );
				//wp_enqueue_script( 'block-slider', get_stylesheet_directory_uri() . '/blocks/slider/slider.js', array(), '1.0.0', true );
			  //},
        ));
        // register a Hero block.
        acf_register_block_type(array(
            'name'              => 'hero',
            'title'             => __('Hero'),
            'description'       => __('A custom heroes block.'),
            'render_template'   => 'blocks/hero/hero.php',
			'category'          => 'formatting',
			'icon' 				=> 'cover-image',
			'align'				=> 'full',
        ));
        // register a Hero text left block.
        acf_register_block_type(array(
            'name'              => 'hero-text-left',
            'title'             => __('Hero Text Left'),
            'description'       => __('A custom heroes Text Left block.'),
            'render_template'   => 'blocks/hero-text-left/hero-text-left.php',
			'category'          => 'formatting',
			'icon' 				=> 'text',
			'align'				=> 'full',
        ));
         // register a tabbed text left block.
         acf_register_block_type(array(
          'name'              => 'tabbed',
          'title'             => __('Tabbed Block'),
          'description'       => __('A Tabbed block.'),
          'render_template'   => 'blocks/tabbed/tabbed.php',
    'category'          => 'formatting',
    'icon' 				=> 'text',
    'align'				=> 'full',
      ));
    }
}  
}
```

### Step 2: Create relatively fields in ACF Pro

Looking to each files and define which fields, subfields need to be added, then add it to your ACF Pro, You can name it whatever you want, the recommend way is using prefix Block:


### Just Using it